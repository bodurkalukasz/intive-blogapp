README.md
===========


### Table of contents

1. Info
2. Requirements
3. Build system and run
4. What should be added in order to become proper beta -> prod version
5. Testing

## 1. Info

This is some kind of alpha version of the app. This could never go to the production at this state :-) Created as an showcase for recruitment process and for fun,  than a real life production app  (even that it is working as it should). 

In section four i have placed some of my thoughts about this app and its further development. I'm happy to talk about those on meeting, if needed.

## 2. Requirements


- node.js
- NPM
- "gulp connect" server for routing tests
- if live prod version: htaccess rewrite engine support (we should retrieve template when user will refresh page in browser or go to /post/:id page directly) - this is not implemented.


## 3. Build system and run

There is no need to build an app becouse it came with ready /DIST build. 
Sure, this should be one build command but you should follow this steps in order to build js/css :

- open terminal and go to project folder
- run "npm install" 
- run "gulp css"
- run "gulp js"
- run "gulp connect" 
- open browser and go to "localhost:8080/dist"

Have fun with blog posts :-)

## 4. What should be added in order to become proper beta -> prod version

Projet is not as finished as i would like to, but in the principle of "Done is better than perfect" i'm sending what i was able to achieve in this limited time. There is no tests included, but i would use QUnit test framework for that (as app is mainly on JQuery). Below i put some of my thoughts about changes in project (if i had more time):

- i tried to put many comments to the code but it could be so much better. Files should have headers with description/version etc for automatic documentation creator
- scss part of project is in little mess... needs atomization and optimalisation, but to be honest there is not much code and I was focusing on to deliver the app than on css optimalisation,
- js code is mainly in main.js which is not so good.. this need to be atomized and optimized
- there is no tests.. sorry i had not enough time to prepare functions for test and to include tests themselves
- Some kind of AMD loader with depedencies support would be nice (RequireJS or just ES6 + babelify).
- Routing lib is vanilla-js nice little code, it is not an React router but works pretty well!:)
- Retrieve route and content on browser refresh, this would be done with help of .htaccess rewrite engine where query like "domain/post/postID" would redirect to /domain with get querry containing query="post/postID" that would be passed to router and proper actions would fire. See /dist/.htaccess for details
- Well... move to webpack build?:)
- More responsive tables for mobile
- Use common libraries form NPM instead of local if needed
- handle loaded posts responsive behavior and design (prepare content in many ways before display to front)
- change more code parts to global-functions with parameters for optimalisation and code maintenance/clearity/reusable and tests handle
- better handler or browserHashHistory (router dont work on it so back/forward button works only from blog post when route ich changed not when user click "back to article list" at bottom of blog post.)
- resolve some silly problem with modal window (when opened few times script freeze)
- set up GULP global build task and watch task with server refresh
- better error handle (as global function on error from network but also on error from webserice)
- loaded thumbnail on post lists is really big image:)
- some weird background bug on postModal in chrome (part of image dissapears)
- Validation is working but needs better styling
- When we change blog url to something else then we got 404 error this sould be handled by take url from htaccess just before app starts and let router decode what should be done. Not implemented, but its simple to do.
- default image when there is no in blog post

## 5. Testing

Sorry, there was no time for unitTests :-(

- Routing works with previous/next browser buttons (go to the post and then previous/next with browser arrows)


