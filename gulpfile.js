var gulp = require('gulp');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var gp_concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso'); //optymize css file - use cssComb instead?
var connect = require('gulp-connect');

var source      = './source/';
var destination = './dist/';

//scss compile

gulp.task('css', function() {
    gulp.src(source + 'scss/**/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(csso({
            restructure: true,
            sourceMap: false,
            debug: false
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(destination + 'css'));
});



//js minify

gulp.task('js', function () {
   gulp.src([
    //TODO this is sad loading this as inline, but when we are doing synchronous load...
        source + '/js/lib/navigo.js',
        source + '/js/lib/jquery.js',
        source + '/js/modules/helpers.js',        
        source + '/js/lib/jquery.validate.js',
        source + '/js/lib/additional-methods.js',
        source + '/js/lib/materialize.js',
        source + '/js/main.js',
        source + '/js/modules/displayPost.js',
        source + '/js/modules/loadingBg.js'
    ])
      .pipe(gp_concat('main.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(destination + 'js'))
});



 //add some autoloader and js/css watch.
gulp.task('connect', function() {
  connect.server();
});
 
gulp.task('default', ['connect']);


//gulp.task('default',function() {
    //gulp.watch(source + 'scss/**/*.scss',['css']);
    //gulp.watch('js/**/*.js',['js']);
//});
