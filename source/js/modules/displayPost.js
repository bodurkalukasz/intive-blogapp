var displayPost = {
  init: function(blogUrl , id) {
		//$('#postWrapper').html('wyswietlam post o id = ' + id);

	   $.ajax({
	    url         : blogUrl + id,
	    type        : "GET",
        error       : function(jqXHR, exception) {

          var message;

          if (jqXHR.status === 0) {
              message = 'Brak połączenia. Sprawdź swoje połączenie z internetem..';
          } else if (jqXHR.status == 404) {
              message = 'Pod wpisanym przez Ciebie adresem nie ma bloga';
          } else if (jqXHR.status == 500) {
              message = 'Błąd Serwera. Skontaktuj się z Administratorem.';
          } else if (exception === 'parsererror') {
              message = 'Błąd danych wejściowych. Skontaktuj się z Administratorem.';
          } else if (exception === 'timeout') {
              message = 'Upłynął limit czasu żądania. Spróbuj ponownie później';
          } else if (exception === 'abort') {
              message = 'Błąd. Spróbuj ponownie później.';
          } else {
              message = 'Nieznany błąd\n' + jqXHR.responseText;
          }
          $("#postWrapper").html(message);

      },
	    success     : function(data){
	      $('.sliderEntry h1').html(data.title);   
	      $('#postWrapper').html(data.content);
        if(data.post_thumbnail === undefined || data.post_thumbnail === null){
           $('.slider').css({'background-image':'url("/dist/img/bg-default.jpg")'});
        }else{
          $('.slider').css({'background-image':'url('+data.post_thumbnail.URL+')'});
        }        
	      $('div').animate({ scrollTop: 0 }, 'slow');
	    }
	  });    


  }
}