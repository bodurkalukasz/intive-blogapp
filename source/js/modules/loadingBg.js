// Init a white background while call a request
var loadingBg = {
  start: function () {
    $(".loading").removeClass('on');
    $(".loading").addClass('on');
  },
  end: function () {
    $(".loading").removeClass('on');
  }
};