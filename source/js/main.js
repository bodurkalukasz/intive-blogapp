/**
 * MainJS File for Application
 *
 * version: 1.0
 * file:    main.js
 * author:  lbodurka
 * description: {{desc}}
 */

$(document).ready(function(){

  //Bind and/or initialize layout elements

  //init material design select
  $('select').material_select();

  //mobile navbar trigger
  $(".button-collapse").sideNav();

  var openPostInModal = {
    init:function(getThisRoute){
      $('.lean-overlay').remove();
         var idToRoute = getThisRoute;

        $('#postModal').openModal({
          dismissible: false, // Modal can be dismissed by clicking outside of the modal
          opacity: .5, // Opacity of modal background
          in_duration: 300, // Transition in duration
          out_duration: 200, // Transition out duration
          starting_top: '0', // Starting top style attribute
          ending_top: '0', // Ending top style attribute
          ready: function() { 
            router.navigate('/post/'+idToRoute);

          }, // 
          complete: function() { 
            $('.lean-overlay').remove();
          } // clear modal
        });
    }
  }

  var bindButtonRoutes = {
    init: function(){

    //Bind modal
    $('.displayPostButton').click(function(e){
      e.preventDefault()
      openPostInModal.init($(this).data('route'));
    });

    }
  }
  

  // Initialize Simple Router and app Routes
  var router = new Navigo();
  //router.navigate('/post/7');

  router
    .on({
      'post/:id': function (parameters) {

      var takeFormUrl     = $('#sitedomain').val(),
          WSapiUrlRoute = 'https://public-api.wordpress.com/rest/v1.1/sites/',
          WSpostsRoute  = '/posts/';

        displayPost.init(WSapiUrlRoute+$('#sitedomain').val()+WSpostsRoute,parameters.id);
        openPostInModal.init(parameters.id);

        $('#closePostModal').click(function(e){
          e.preventDefault();
          router.navigate('./');
          $('#postModal').closeModal();
          $('.lean-overlay').remove();
          $('#postWrapper').html('Wczytuję treść...');
        });

      },
      '*': function () {
        //hello, youre on home page
        console.log('hp');
        $('#postModal').closeModal();

      }
    })
    .resolve();

  

// validator new mothod that will not allow http or www at the begining. 
// We can of course trim this later and allow user to put any kind of url
$.validator.addMethod("url_no_www", function(value, element) { 
    return /^[^(?:http)?:\/\/)?(?:www)?].*$/g.test(value); 
}, "Błędny Adres (proszę nie używać prefixów http ani www)");


  //Live validate plugin
  $("#mainAppForm").validate({
    // Specify validation rules
    rules: {
      sitedomain: {
        required: true,
        //url:true,
        url_no_www:true,
        maxlength: 100
      },      
      searchquery: {
        required: true,
        maxlength: 100
      },
      responseqty: {
        required: true,
        range: [5, 100]
      }
    },
    // Specify validation error messages
    messages: {
      sitedomain: {
        required: "To pole jest wymagane",
        url_no_www: "Błędny adres (proszę nie używać prefixów http ani www)",
        maxlength: "To pole nie może być dłuższe niż 100znaków"
      },
      responseqty: "Wartość powinna być pomiędzy 5-100",
      searchquery: {
        required: "To pole jest wymagane",
        maxlength: "To pole nie może być dłuższe niż 100znaków"
      }
    }
  });

  // Bind form button for submit if form.valid and validate form and show errors when form.invalid.
  $('#goForm').click(function(e){
    e.preventDefault();
    //check if form is valid, if now display errors else send form.
    if($('#mainAppForm').valid() === true){
      $(".errorBox").html('');
      loadingBg.start();

      //we could do serialize on ajax.data but lets take this values seperatly
      var takeFormUrl     = $('#sitedomain').val(),
          takeFormQuery   = $('#searchquery').val(),
          takeFormQty     = $('#responseqty').val(),
          takeFormSort    = $('#howtosort').val(),
          WSapiUrl = 'https://public-api.wordpress.com/rest/v1.1/sites/',
          WSposts  = '/posts';

      $.ajax({
        url         : WSapiUrl+takeFormUrl+WSposts+'?search='+takeFormQuery+'&number='+takeFormQty+'&order_by='+takeFormSort,
        type        : "GET",
        error       : function(jqXHR, exception) {

          var message;

          if (jqXHR.status === 0) {
              message = 'Brak połączenia. Sprawdź swoje połączenie z internetem..';
          } else if (jqXHR.status == 404) {
              message = 'Pod wpisanym przez Ciebie adresem nie ma bloga';
          } else if (jqXHR.status == 500) {
              message = 'Błąd Serwera. Skontaktuj się z Administratorem.';
          } else if (exception === 'parsererror') {
              message = 'Błąd danych wejściowych. Skontaktuj się z Administratorem.';
          } else if (exception === 'timeout') {
              message = 'Upłynął limit czasu żądania. Spróbuj ponownie później';
          } else if (exception === 'abort') {
              message = 'Błąd. Spróbuj ponownie później.';
          } else {
              message = 'Nieznany błąd\n' + jqXHR.responseText;
          }
          $(".errorBox").html('<div class="card-panel red accent-1">'+message+'</div>');
          loadingBg.end();

      },
        success     : function(data){
          
          //check if data.error is not empty

          var restPostsList = data.posts.map(function(item){

              /* handle empry nodes 
              TODO, change to some global function:
              {{{{use: checkIfEmpty(value, type) }}}}
              {{{{ resp: notempty:same value; empty: placeholder for sent type }}}}}
              */
              var itemPhoto = '';
              //not all posts have photo, so if no photo at json, print placeholder
              if(item.post_thumbnail === undefined || item.post_thumbnail === null){
                itemPhoto = 'http://placehold.it/100x80';
              }else{
                itemPhoto = item.post_thumbnail.URL;
              }

              return '<tr>'+
              '<td><img src="'+itemPhoto+'" alt="" width="100px"/></td>'+
              '<td>'+item.title+'</td>'+
              '<td>'+item.discussion.comment_count+'</td>'+
              '<td><button class="displayPostButton waves-effect waves-light btn" data-route="'+item.ID+'">Czytaj ten wpis</button></td>'+
              '</tr>'
              });

          var respListTemplate = ''+
            '<table class="striped highlight">'+
            '<thead>'+
            '  <tr>'+
            '      <th>Zdjęcie</th>'+
            '      <th>Tytuł</th>'+
            '      <th>Ilosć komentarzy</th>'+
            '      <th>Opcje</th>'+
            '  </tr>'+
            '</thead>'+
            '<tbody>'+restPostsList.join("")+
            '</tbody>'+
            '</table>'
          ;

          $('#postListContainer').html(respListTemplate);
          bindButtonRoutes.init();
          loadingBg.end();
        }
      });    
    }else{
      loadingBg.end();
      $('#mainAppForm').valid();
    }
  });


});
